const path = require("path")
const baseConfig = require("../build/webpack.base.conf.js")
const merge = require("webpack-merge")
const packageConfig = require("../package.json")
const chalk = require("chalk")

module.exports = {
  /**
   * Name of your design system. Changes both page title and sidebar logo.
   */
  title: "Pentia Design System",
  /**
   * Most of the styles are defined in /docs/docs.styles.scss
   */
  version: packageConfig.version,
  theme: {
    maxWidth: "100%",
    sidebarWidth: 300,
    fontFamily: {
      base: ["'Fira Sans'", "Helvetica", "Arial", "sans-serif"],
      monospace: ["Consolas", "'Liberation Mono'", "Menlo", "monospace"],
    },
  },
  renderRootJsx: path.join(__dirname, "../docs/components/Preview.js"),
  /**
   * Define a custom code highlighting theme.
   */
  editorConfig: {
    theme: "night",
  },
  /**
   * Path to static assets directory
   */
  assetsDir: path.join(__dirname, "../src/assets"),
  /**
   * Enabling the below option will break things in Vue Design System!
   */
  skipComponentsWithoutExample: false,
  /**
   * We’re defining below JS and SCSS requires for the documentation.
   */
  require: [
    path.join(__dirname, "../docs/docs.helper.js"),
    path.join(__dirname, "../docs/docs.styles.scss"),
  ],
  /**
   * Enabling the following option splits sections into separate views.
   */
  pagePerSection: true,
  sections: [
    {
      name: "Getting Started",
      content: "../docs/01-getting-started.md",
      // Needs to be loaded in somewhere as this is also shown in
      // element, Pattern & Template overviews.
      components: "../docs/components/status/**/[A-Z]*.vue",
      sectionDepth: 1,
      exampleMode: "hide",
      usageMode: "hide",
    },
    {
      name: "Atoms",
      content: "../docs/_patterns/00-atoms.md",
      components: "../src/_patterns/00-atoms/**/[A-Z]*.vue",
      exampleMode: "expand",
      usageMode: "expand",
      sectionDepth: 2,
    },
    {
      name: "Molecules",
      content: "../docs/_patterns/01-molecules.md",
      components: "../src/_patterns/01-molecules/**/[A-Z]*.vue",
      exampleMode: "expand",
      usageMode: "expand",
      sectionDepth: 2,
    },
    {
      name: "Organisms",
      content: "../docs/_patterns/02-organisms.md",
      components: "../src/_patterns/02-organisms/**/[A-Z]*.vue",
      exampleMode: "expand",
      usageMode: "expand",
      sectionDepth: 2,
    },
    {
      name: "Templates",
      content: "../docs/_patterns/03-templates.md",
      components: "../src/_patterns/03-templates/**/[A-Z]*.vue",
      exampleMode: "expand",
      usageMode: "expand",
      sectionDepth: 2,
    },
    {
      name: "Pages",
      content: "../docs/_patterns/04-pages.md",
      components: "../src/_patterns/04-pages/**/[A-Z]*.vue",
      exampleMode: "expand",
      usageMode: "expand",
      sectionDepth: 2,
    },
    {
      name: "Voice & Tone",
      content: "../docs/02-voice-and-tone.md",
      sectionDepth: 1,
      exampleMode: "hide",
      usageMode: "hide",
    },
    {
      name: "FAQ",
      content: "../docs/03-faq.md",
      sectionDepth: 1,
      exampleMode: "hide",
      usageMode: "hide",
    },
    {
      /**
       * Private components have to be loaded into the documentation as well,
       * otherwise anything using them will be broken. We’re loading them in
       * their own section, which then gets hidden in docs/docs.styles.scss
       */
      name: "Private Components",
      exampleMode: "hide",
      usageMode: "hide",
      components: "../src/**/[_]*.vue",
    },
  ],
  /**
   * Custom wrapper template for the documentation.
   */
  template: {
    title: "Rejsekortet Design System",
    lang: "en",
    trimWhitespace: true,
    head: {
      meta: [
        {
          name: "viewport",
          content: "width=device-width,initial-scale=1.0",
        },
        {
          name: "format-detection",
          content: "telephone=no",
        },
      ],
    },
  },
  /**
   * Ignore app.vue, tests, and example component.
   */
  ignore: [
    "**/App.vue",
    "**/__tests__/**",
    "**/*.test.js",
    "**/*.test.jsx",
    "**/*.spec.js",
    "**/*.spec.jsx",
    "**/ExampleComponent.vue",
  ],
  webpackConfig: merge(baseConfig, {
    module: {
      rules: [
        {
          test: /\.(css?|scss|sass)(\?.*)?$/,
          use: [
            "style-loader",
            "css-loader",
            "postcss-loader",
            "sass-loader",
            {
              loader: "sass-resources-loader",
              options: {
                resources: [
                  path.join(__dirname, "../src/assets/tokens/tokens.scss"),
                  path.join(__dirname, "../src/assets/tokens/tokens.map.scss"),
                  path.join(__dirname, "../src/styles/styles.scss"),
                ],
              },
            },
          ],
        },
      ],
    },
  }),
  styleguideDir: "../dist/docs",
  printServerInstructions() {},
  printBuildInstructions(config) {
    console.log(chalk.cyan("\n  Design System Docs build finished succesfully!\n"))
    console.log(
      chalk.yellow(
        "  Tip: You can now deploy the docs as a static website.\n" +
          "  Copy the build files from " +
          `${config.styleguideDir}\n`
      )
    )
  },
  /**
   * Configure docs server to redirect asset queries
   */
  // configureServer(app) {
  //   // `app` is the instance of the express server running the docs
  //   app.get("/assets/:file", (req, res) => {
  //     res.redirect(req.params.file)
  //   })
  // },
}
