### Through this guide you will learn how to apply Vue Design System’s voice and choose the right tone. Using the right voice and tone is important as it allows us to better connect with our users.

You’re looking at Pentia Design Systems <code>voice & tone</code>. Everything you see here is editable in Markdown format. To change or remove this content, see [/docs/05-voice-and-tone.md].

## Crisp and clear

To edit or remove this guideline, see [/docs/voice-and-tone.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## Empathetic

Insert relevant text to describe this section

## Confident, but not arrogant

Insert relevant text to describe this section
